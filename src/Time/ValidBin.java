/**
 * @author jhkim
 * ValidBin.java
 * 2017. 6. 14.
 */
package Time;

import java.sql.Timestamp;

public class ValidBin {
    public static void main(String... args) {
        long curTime = System.currentTimeMillis();
        long maxBin = curTime / 3600000;
        long minBin = maxBin - 168;
        long recvBin = 1497427223L / 3600;
        System.out.println("curTime- " + curTime);
        System.out.printf("maxBin: %d, minBin: %d\n", maxBin, minBin);
        System.out.printf("recvBin: %d\n", recvBin);
        System.out.println(recvBin < minBin);
        
        System.out.println(new Timestamp(maxBin * 3600000));
        System.out.println(new Timestamp(minBin * 3600000));
        System.out.println(new Timestamp(418489L * 3600000));
    }
}

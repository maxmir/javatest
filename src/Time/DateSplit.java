/** 
 * @author jhkim
 * @date [2017. 11. 17.] 오전 11:27:35
 * DateSplit.java
 */
package Time;

public class DateSplit {
    public static void main(String[] args) {
        String zoneDate = "2017-11-07 15:24:38.11111+09";
        String convDateTime = convZoneDate(zoneDate);
        System.out.println(convDateTime);
    }

    /** 
     * @author jhkim
     * @date [2017. 11. 17.] 오전 11:48:52
     *
     * @param zoneDate
     * @return
     */
    private static String convZoneDate(String zoneDate) {
        String[] zoneDateArr = zoneDate.split("\\.")[0].split("\\s+");
        String date = zoneDateArr[0];
        String time = zoneDateArr[1];
        String[] dateArr = date.split("\\-");
        StringBuffer convDateTime = new StringBuffer();
        for (String dateStr : dateArr) convDateTime.append(dateStr);
        String[] timeArr = time.split("\\.")[0].split("\\:");
        for (String timeStr : timeArr) convDateTime.append(timeStr);
        return convDateTime.toString();
    }
    
}

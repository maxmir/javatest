package Time;

import java.text.SimpleDateFormat;
import java.util.Date;


public class SimpleDate {
    public static void main(String[] args) {
//        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
        String pattern1 = "MMM dd yyyy HH:mm:ss";
        String pattern2 = "yyyyMMddHHmmss";
        String pattern3 = "yyyy-MM-dd-HH-mm-ss-SSS";
        SimpleDateFormat sdf = new SimpleDateFormat(pattern3);
        Date date = new Date();
//        String today = sdf.format(date);
        String todayMonth = sdf.format(date);
        String nextMonth = sdf.format(date);
        
        System.out.println(todayMonth);
        date.setMonth(3);
        System.out.println(date);
    }
}

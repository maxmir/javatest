/** 
 * @author jhkim
 * @date [2017. 8. 29.] 오후 4:30:08
 * TimeStampExample.java
 */
package Time;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;

public class TimeStampExample {
    
    public static void main(String[] args) {
        //method 1
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        System.out.println("timestamp- " + timestamp);

        //method 2 - via Date
        Date date = new Date();
        System.out.println(new Timestamp(date.getTime()));

        //return number of milliseconds since January 1, 1970, 00:00:00 GMT
        System.out.println("getTime-" + timestamp.getTime());

        //format timestamp
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss");
        System.out.println("format-" + sdf.format(timestamp));
    }
}

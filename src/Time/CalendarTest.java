/**
 * @author jhkim
 * CalendarTest.java
 * 2017. 6. 26.
 */
package Time;

import java.util.Calendar;

public class CalendarTest {
    public static void main(String[] args) {
        Calendar c1 = Calendar.getInstance();
        Calendar c2 = Calendar.getInstance();
        System.out.println(c1.getTimeInMillis());
        c2.setTimeInMillis(1498457595707L);
        System.out.println(c1.get(Calendar.DAY_OF_YEAR));
        System.out.println(c2.get(Calendar.DAY_OF_YEAR));
    }
}

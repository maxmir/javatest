package Time;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class getTodayDate {
	public static void main(String[] args) {
		Calendar cal= Calendar.getInstance();
		
		// 년 구함
		System.out.println("Calendar.YEAR..."+cal.get(Calendar.YEAR));
		
		// 월 구함, 1월이 0임, 2월:1, 3월:2.....
		int month = cal.get(Calendar.MONTH) + 1;
		System.out.println("Calendar.MONTH...."+ month);
		
		// 일을 구함, 1일, 2일, 3일...
		System.out.println("Calendar.DAY_OF_MONTH..."+cal.get(Calendar.DAY_OF_MONTH)); 
		
		// 24시간 기준으로 시간을 구한다. 
		System.out.println("Calendar.HOUR_OF_DAY...."+ cal.get(Calendar.HOUR_OF_DAY)); 
		
		//12시간 기준으로 시간을 구한다
		System.out.println("Calendar.HOUR...."+ cal.get(Calendar.HOUR)); 
		
		System.out.println();
		
		String currentDate1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date());
		String currentDate2 = new SimpleDateFormat("yyyy-MM-dd 00:00:00").format(new java.util.Date());
		String currentDate3 = new SimpleDateFormat("yyyy-MM-dd").format(new java.util.Date());
		System.out.println(currentDate1);
        System.out.println(currentDate2);
        System.out.println(currentDate3);

	}
}

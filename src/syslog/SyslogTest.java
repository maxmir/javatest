package syslog;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;
import org.productivity.java.syslog4j.Syslog;
import org.productivity.java.syslog4j.SyslogConfigIF;
import org.productivity.java.syslog4j.SyslogIF;
import org.productivity.java.syslog4j.impl.net.tcp.TCPNetSyslogConfig;
import org.productivity.java.syslog4j.impl.net.udp.UDPNetSyslogConfig;
import org.productivity.java.syslog4j.util.SyslogUtility;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import MyBatis.SqlMapClient;

/**
 * Created by jhkim on 2017. 4. 18..
 */
public class SyslogTest {
private static final Logger logger = LoggerFactory.getLogger(SyslogTest.class);
    
    private static String name = "notebook";
    private static String host = "172.16.100.102";
    private static String protocol = "udp";
    private static Integer port = 514;

    public void syslogtest() {
        SyslogConfigIF config = null;
        SyslogIF client = null;
        
        config = (protocol.equals("tcp")) ? new TCPNetSyslogConfig(host, port) : new UDPNetSyslogConfig(host, port);
        client = Syslog.createInstance(name, config);
        
        SqlSession session = SqlMapClient.getSqlSession();
        List<Map<String, Object>> cqList = session.selectList("CQDQ.getCQ");
        List<Map<String, Object>> dqList = session.selectList("CQDQ.getDQ");
        session.close();
        
        for (Map<String, Object> map : cqList) {
            System.out.println(map);
        }
        
//        client.info("Information 1");
//        logger.info("{}", SyslogUtility.getLocalName());
//        SyslogUtility.sleep(500l);
//        client.info("Information 2");
//        SyslogUtility.sleep(700l);
//        client.info("Information 3");
//        client.warn("Warning 4");
    }
    
    public static void main(String[] args) {
        SyslogTest slt = new SyslogTest();
        slt.syslogtest();
    }
}

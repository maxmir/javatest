/**
 * @author jhkim
 * ConnTest.java
 * 2017. 4. 20.
 */
package MyBatis;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;

public class ConnTest {
    public static void main(String[] args) {
        SqlSession session = SqlMapClient.getSqlSession();
        List<Map<String, Object>> list = session.selectList("ConnTest.getDQ");
        System.out.println("list test : " + list.get(0));
        session.close();
    }
}

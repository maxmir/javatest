/**
 * @author jhkim
 * SqlMapClient.java
 * 2017. 4. 20.
 */
package MyBatis;

import java.io.Reader;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

public class SqlMapClient {
    private static SqlSession session;
    
    static {
        try {
            String resource = "main/resources/conf/myBatisConfig.xml";
            Reader reader = Resources.getResourceAsReader(resource);
            SqlSessionFactory sqlMapper = new SqlSessionFactoryBuilder().build(reader);
            
            session = sqlMapper.openSession();
            
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public static SqlSession getSqlSession() {
        return session;
    }
}

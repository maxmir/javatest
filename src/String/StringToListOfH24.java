package String;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class StringToListOfH24 {
    public static void main(String[] args) {
//        String dataSetStr = "03|995.1||||1|수정|원부자료없음,09||994||1017.9|2|수정|DB삭제,15||995||1018.6|2|수정|DB삭제,21||996.3||1020.4|2|수정|DB삭제";
        String dataSetStr = "03|-20.1|-4.3|1|수정|자기지,04|-21.3|-3.8|1|수정|자기지,06|-22.7|-4.1|1|수정|자기지,07|-23.5|-3.8|1|수정|자기지,08|-21.3|-2.3|1|수정|자기지";
        String[] dataSetArr = dataSetStr.split(",");
        
        List<List<HashMap<Integer, String>>> dataSetList = new ArrayList<List<HashMap<Integer, String>>>();
        
        System.out.println(dataSetStr);
        for (int i = 0; i < dataSetArr.length; i++) {
            System.out.println("---------- dataSetArr " + (i + 1) + " ----------");
            dataSetList.add(createHashMap(dataSetArr, i, 5));
        }
        System.out.println(dataSetList);
    }

    private static List<HashMap<Integer, String>> createHashMap(String[] dataSetArr, int curPos, int procRsltPos) {
        ArrayList<HashMap<Integer, String>> al = new ArrayList<HashMap<Integer,String>>();
        HashMap<Integer, String> hm = new HashMap<Integer, String>();
        String[] dataArr = null;
        
        for (int j = 0 + 1; j < 24; j++) {
            for (int k = 0; k < dataSetArr.length; k++) {
                dataArr = dataSetArr[k].split("\\|");
                
                if (j == Integer.valueOf(dataArr[0])) {
                    if (dataArr[curPos+1].length() > 0) {
                        hm.put(j, dataArr[curPos + 1] + "/" + dataArr[procRsltPos]);
                        break;
                    }
                } else if (k == dataSetArr.length - 1) {
                    hm.put(j, "");
                }
            }
            
        }
        al.add(hm);
        hm = new HashMap<Integer, String>();
        System.out.println(al + "\n");
        return al;
    }
}

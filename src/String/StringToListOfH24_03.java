package String;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class StringToListOfH24_03 {
    public static void main(String[] args) {
        String dataSetStr = "03|993.1||||1|수정|원부자료없음^09||996.3||1020.2|2|수정|DB삭제^15||990.2||1012.8|2|수정|DB삭제^21||992.7||1016|2|수정|DB삭제";
        String[] dataSetArr = dataSetStr.split("\\^");
        
        System.out.println();
        System.out.println(dataSetStr);
        List<List<HashMap<Integer, String>>> dataSetList = createList(dataSetArr, 4, 7);
//        System.out.println(dataSetList);
    }

    private static List<List<HashMap<Integer, String>>> createList(String[] dataSetArr, int dataCount, int procRsltPos) {
        List<List<HashMap<Integer, String>>> dataSetList = new ArrayList<List<HashMap<Integer, String>>>();
        for (int i = 0; i < 2; i++) {
            System.out.println("---------- dataSetArr " + (i + 1) + " ----------");
            dataSetList.add(createHashMap(dataSetArr, i, procRsltPos));
        }
        return dataSetList;
    }

    private static List<HashMap<Integer, String>> createHashMap(String[] dataSetArr, int curDataPos, int procRsltPos) {
        ArrayList<HashMap<Integer, String>> al = new ArrayList<HashMap<Integer,String>>();
        HashMap<Integer, String> hm = new HashMap<Integer, String>();
        String[] dataArr = null;
        
        for (int j = 0; j < 24; j++) {
            for (int k = 0; k < dataSetArr.length; k++) {
                dataArr = dataSetArr[k].split("\\|");
                
                if (j == Integer.valueOf(dataArr[0])) {
                    if (dataArr[curDataPos+1].length() > 0) {
                        hm.put(j, dataArr[curDataPos + 1] + "/" + dataArr[procRsltPos]);
                        break;
                    }
                } else if (k == dataSetArr.length - 1) {
                    hm.put(j, "");
                }
            }
        }
        System.out.println(hm);
        al.add(hm);
        hm = new HashMap<Integer, String>();
        return al;
    }
}

/** 
 * @author jhkim
 * @date [2017. 12. 5.] 오전 11:51:35
 * VersionCompare.java
 */
package String;

import java.util.ArrayList;
import java.util.List;

public class VersionCompare {
    public static void main(String[] args) {
        String v1 = "5.7";
        String v2 = "5.5";
        String v3 = "5.5.7";
        String v4 = "6.4";
        String v5 = "6.1.5";
        String v6 = "6.4.2";
        List<String> li = new ArrayList<>();
        li.add(v1);
        li.add(v2);
        li.add(v3);
        li.add(v4);
        li.add(v5);
        li.add(v6);
        
        String compareVersion = "1";
        String newestVersion = "";
        
        // 만약 newestVersion에 값이 없다면
        // 입력값과 compareString을 비교하여 compareString보다 같거나 큰 지 확인한다.
        // compareString을 '.'으로 split하여 갯수만큼 차례로 비교하여 
        // 같거나 큰 버전을 찾으면 newestVersion에 대입한다
        // 만약 newestVersion 값이 있다면
        // 입력값과 newestVersion을 비교하여 newestVersion보다 같거나 큰 지 확인한다.
        // compareString을 '.'으로 split하여 갯수만큼 차례로 비교하여 
        // 같거나 큰 버전을 찾으면 newestVersion에 대입한다
        for (String v : li) {
            if ("".equals(newestVersion) == false) {
                compareVersion = newestVersion;
            }
            newestVersion = getNewsetVersion(v, compareVersion, newestVersion);
            System.out.println("newestVersion-> '" + newestVersion + "'");
        }
    }

    /** 
     * @author jhkim
     * @date [2017. 12. 5.] 오후 12:27:49
     *
     * @param inputVersion
     * @param compareVersion
     * @param newestVersion 
     */
    private static String getNewsetVersion(String inputVersion, String compareVersion, String newestVersion) {
        String[] iva = inputVersion.split("\\.");
        String[] csa = compareVersion.split("\\.");

        try {
            for (int i = 0; i < csa.length; i++) {
                int ivn = Integer.valueOf(iva[i]);
                int csn = Integer.valueOf(csa[i]);
                if (ivn > csn) {
                    return inputVersion;
                }
                else if (ivn < csn) {
                    return newestVersion;
                }
            }
        } catch (ArrayIndexOutOfBoundsException e) {
            // 일정부분까지 일치하는 값이지만 검사할 값의 자리수가 더 많을 때
            e.printStackTrace();
            return newestVersion;
        }
        // 두값이 일치할때
        return inputVersion;
    }
}

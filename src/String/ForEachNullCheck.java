package String;

/**
 * @author jhkim
 * ForEachNullCheck
 * 2017. 2. 13.
 */
public class ForEachNullCheck {
    public static void main(String... args) {
        String[] testStr = {null, "b", "c"};
//        String[] testStr = null;
        if (testStr != null) {
            for (String string : testStr) {
                System.out.println(string);
            }
        }
    }
}

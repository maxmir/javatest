package String;

public class URISplitTest {
	public static void main(String[] args) {
		String reqURI = "/web/main.do/ ";
		String[] uriSplit = reqURI.split("\\/");
		int cnt = 0;
		for (String string : uriSplit) {
			System.out.println(cnt++ + " - " + string);
		}
	}
}

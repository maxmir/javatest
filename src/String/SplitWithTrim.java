/** 
 * @author jhkim
 * @date [2017. 9. 19.] 오후 4:10:17
 * SplitWithTrim.java
 */
package String;

import java.util.ArrayList;
import java.util.Arrays;

public class SplitWithTrim {
    public static void main(String[] args) {
        String deviceIPs = "172.16.100.201  ,   172.16.100.254";
        ArrayList<String> deviceIpList = new ArrayList<>(Arrays.asList(deviceIPs.split("\\s*,\\s*")));
        for (String dIp : deviceIpList) {
            System.out.printf("[%s]", dIp);
        }
    }
}

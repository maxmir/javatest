/** 
 * @author jhkim
 * @date [2017. 12. 7.] 오후 3:22:52
 * StringEncode.java
 */
package Encoding;

import java.io.UnsupportedEncodingException;

public class StringEncode {
    public static void main(String[] args) {
        String encoding = "UTF-8";
        String str = "";
        try {
            str = new String(("file_name1").getBytes(encoding));
            System.out.println(str);
            str = new String(("file_name1").getBytes(encoding), encoding);
            System.out.println(str);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }
}

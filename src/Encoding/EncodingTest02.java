package Encoding;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;


public class EncodingTest02 {
    public static void main(String[] args) throws UnsupportedEncodingException {
        String s = "";
        byte[] BOM = new byte[4];
        BOM = s.getBytes();
                             
        // 3. 파일 인코딩 확인하기
        if( (BOM[0] & 0xFF) == 0xEF && (BOM[1] & 0xFF) == 0xBB && (BOM[2] & 0xFF) == 0xBF )
            System.out.println("UTF-8");
        else if( (BOM[0] & 0xFF) == 0xFE && (BOM[1] & 0xFF) == 0xFF )
            System.out.println("UTF-16BE");
        else if( (BOM[0] & 0xFF) == 0xFF && (BOM[1] & 0xFF) == 0xFE )
            System.out.println("UTF-16LE");
        else if( (BOM[0] & 0xFF) == 0x00 && (BOM[1] & 0xFF) == 0x00 && 
                 (BOM[0] & 0xFF) == 0xFE && (BOM[1] & 0xFF) == 0xFF )
            System.out.println("UTF-32BE");
        else if( (BOM[0] & 0xFF) == 0xFF && (BOM[1] & 0xFF) == 0xFE && 
                 (BOM[0] & 0xFF) == 0x00 && (BOM[1] & 0xFF) == 0x00 )
            System.out.println("UTF-32LE");
        else
            System.out.println("EUC-KR");
    }
}

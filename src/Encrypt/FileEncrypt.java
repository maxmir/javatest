/** 
 * @author jhkim
 * @date [2017. 12. 7.] 오전 10:08:06
 * FileEncrypt.java
 */
package Encrypt;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.apache.commons.codec.digest.DigestUtils;

public class FileEncrypt {
    public static void main(String[] args) {
        try {
            String fileDir1 = "/Users/jhkim/Temp";
            String fileName1 = "InstallCert.java";
            String fileSeparator = System.getProperty("file.separator");
            Path filePath1 = Paths.get(fileDir1 + fileSeparator + fileName1);
            String encrypt = DigestUtils.sha256Hex(Files.readAllBytes(filePath1));
            System.out.println(encrypt);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

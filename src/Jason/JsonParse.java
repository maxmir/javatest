package Jason;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

public class JsonParse {
    public static void main(String[] args) {
        new JsonParse().parse();
    }

    void parse() {
        String isr = "[{\"menu_order\":1,\"parent_menu_seq\":\"\",\"menu_seq\":\"02\",\"menu_url\":\"/scenarioManage/index.do\",\"menu_nm\":\"시나리오 관리\"},{\"menu_order\":1,\"parent_menu_seq\":\"02\",\"menu_seq\":\"0201\",\"menu_url\":\"/scenarioManage/scenarioList.do\",\"menu_nm\":\"시나리오 목록\"},{\"menu_order\":2,\"parent_menu_seq\":\"02\",\"menu_seq\":\"0202\",\"menu_url\":\"/scenarioManage/scenarioCreateUpdate.do\",\"menu_nm\":\"시나리오 생성/수정\"},{\"menu_order\":2,\"parent_menu_seq\":\"\",\"menu_seq\":\"03\",\"menu_url\":\"/metasploit/dashboard/index.do\",\"menu_nm\":\"행위발생 관리\"},{\"menu_order\":1,\"parent_menu_seq\":\"03\",\"menu_seq\":\"0301\",\"menu_url\":\"\",\"menu_nm\":\"위협발생 관리\"},{\"menu_order\":1,\"parent_menu_seq\":\"0301\",\"menu_seq\":\"030101\",\"menu_url\":\"/metasploit/index.do\",\"menu_nm\":\"타겟 호스트\"},{\"menu_order\":2,\"parent_menu_seq\":\"0301\",\"menu_seq\":\"030102\",\"menu_url\":\"/metasploit/network_scan.do\",\"menu_nm\":\"자동 호스트 등록(스캔)\"},{\"menu_order\":3,\"parent_menu_seq\":\"0301\",\"menu_seq\":\"030103\",\"menu_url\":\"/metasploit/exploit_setting.do\",\"menu_nm\":\"공격 설정\"},{\"menu_order\":4,\"parent_menu_seq\":\"0301\",\"menu_seq\":\"030104\",\"menu_url\":\"/metasploit/exploit_result.do\",\"menu_nm\":\"공격 이력 결과\"},{\"menu_order\":5,\"parent_menu_seq\":\"0301\",\"menu_seq\":\"030105\",\"menu_url\":\"/metasploit/session_list.do\",\"menu_nm\":\"세션 관리\"},{\"menu_order\":2,\"parent_menu_seq\":\"03\",\"menu_seq\":\"0302\",\"menu_url\":\"\",\"menu_nm\":\"정상행위발생 관리\"},{\"menu_order\":1,\"parent_menu_seq\":\"0302\",\"menu_seq\":\"030201\",\"menu_url\":\"/act/webAgent/index.do\",\"menu_nm\":\"웹 행위\"},{\"menu_order\":2,\"parent_menu_seq\":\"0302\",\"menu_seq\":\"030202\",\"menu_url\":\"/act/mailAgent/index.do\",\"menu_nm\":\"메일 행위\"},{\"menu_order\":3,\"parent_menu_seq\":\"0302\",\"menu_seq\":\"030203\",\"menu_url\":\"/act/dataAgent/index.do\",\"menu_nm\":\"자료 연동\"},{\"menu_order\":4,\"parent_menu_seq\":\"0302\",\"menu_seq\":\"030204\",\"menu_url\":\"/act/agentInfo/index.do\",\"menu_nm\":\"에이전트 관리\"},{\"menu_order\":3,\"parent_menu_seq\":\"\",\"menu_seq\":\"04\",\"menu_url\":\"/analysis/analysisSetConfig.do\",\"menu_nm\":\"훈련검증/분석\"},{\"menu_order\":1,\"parent_menu_seq\":\"04\",\"menu_seq\":\"0401\",\"menu_url\":\"\",\"menu_nm\":\"자원수집 설정\"},{\"menu_order\":1,\"parent_menu_seq\":\"0401\",\"menu_seq\":\"040101\",\"menu_url\":\"/analysis/analysisSetConfig.do\",\"menu_nm\":\"시나리오별 수집 설정\"},{\"menu_order\":2,\"parent_menu_seq\":\"04\",\"menu_seq\":\"0402\",\"menu_url\":\"\",\"menu_nm\":\"자원수집 현황\"},{\"menu_order\":1,\"parent_menu_seq\":\"0402\",\"menu_seq\":\"040201\",\"menu_url\":\"/analysis/analysisStatusList.do\",\"menu_nm\":\"시나리오별 수집 현황\"},{\"menu_order\":2,\"parent_menu_seq\":\"0402\",\"menu_seq\":\"040202\",\"menu_url\":\"/analysis/analysisScenarioList.do\",\"menu_nm\":\"시나리오별 분석 현황\"},{\"menu_order\":3,\"parent_menu_seq\":\"0402\",\"menu_seq\":\"040203\",\"menu_url\":\"/analysis/analysisScenarioAlarmList.do\",\"menu_nm\":\"시나리오별 알람관리\"},{\"menu_order\":4,\"parent_menu_seq\":\"\",\"menu_seq\":\"05\",\"menu_url\":\"/systemManage/index.do\",\"menu_nm\":\"관리\"},{\"menu_order\":1,\"parent_menu_seq\":\"05\",\"menu_seq\":\"0501\",\"menu_url\":\"\",\"menu_nm\":\"사용자 관리\"},{\"menu_order\":1,\"parent_menu_seq\":\"0501\",\"menu_seq\":\"050101\",\"menu_url\":\"/systemManage/accountManage.do\",\"menu_nm\":\"계정 관리\"},{\"menu_order\":2,\"parent_menu_seq\":\"0501\",\"menu_seq\":\"050102\",\"menu_url\":\"/systemManage/organizationManage.do\",\"menu_nm\":\"조직 관리\"},{\"menu_order\":2,\"parent_menu_seq\":\"0501\",\"menu_seq\":\"050103\",\"menu_url\":\"/systemManage/authorizationManage.do\",\"menu_nm\":\"권한 관리\"}]";
        JSONArray menuArray = (JSONArray) JSONValue.parse(isr);

        for (int i = 0; i < menuArray.size(); i++) {
            JSONObject head = (JSONObject) menuArray.get(i);
            System.out.print(head.get("menu_order").toString()+" ");
            System.out.print(head.get("menu_seq").toString()+"\t\t");
            System.out.print(head.get("menu_url").toString()+"\t\t");
            System.out.println(head.get("menu_nm").toString());
        }
    }

}

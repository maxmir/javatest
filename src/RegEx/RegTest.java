package RegEx;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegTest {
    public static void main(String[] args) {
        String regExpStr = "allows (.*?) to (.*?) via";
        String str = "ip_input.c in BSD-derived TCP/IP implementations allows remote attackers to cause a denial of service (crash or hang) via aaaaaaaa.";
        
        Matcher m = Pattern.compile(regExpStr).matcher(str);
        String[] strArr = null;
        
        
        while (m.find()) {
//            for (int i = 0; i < m.groupCount(); i++) {
//                System.out.println(m.group(i));
//            }
            System.out.println("Found: " + m.group(0));
            String eachStr = m.group(0).substring(6);
            strArr = eachStr.split("to");
            String strArr0 = strArr[0].trim();
            String[] locationActorArr = strArr0.split("\\s+");
            String location = locationActorArr[0];
            String actor = locationActorArr[1];
            
            String strArr1 = strArr[1].trim();
            String[] behaviorKeywordArr = strArr1.split("\\s+");
            String behavior = behaviorKeywordArr[0].trim();
            String exceptBehavior = strArr1.split(behavior)[1].trim();
            String remainSentence = exceptBehavior.split(" via")[0].trim();
            
            if (remainSentence.contains("a denial of service")) {
                System.out.println(remainSentence);
            }
            
            
            
            break;
        }
        
//        for (String string : strArr) {
//            System.out.println(string);
//        }
        
        
//        String regExpStr = "^a.c$";
//        
//        String[] strArr = {"abc", "acc", "adc", "aec", "afc", "agc", "ahc", "aiic"};
//        
//        for(String str : strArr) {
//         System.out.println("[" + str + "] : " + str.matches(regExpStr));
//        }
//        
//        // email 형식 정규식
//        // 개인적으로 해보니 ^$는 꼭 써줘야 겠음
//        String regExpStr2 = "^[a-zA-Z0-9]+[@][a-zA-Z0-9]+[\\.][a-zA-Z0-9]+$";
//        
//        String emailStr = "babolsk@bbb.com";
//        
//        Pattern pattern = Pattern.compile(regExpStr2);
//        
//        Matcher match = pattern.matcher(emailStr);
//        
//        // 매칭에 맞을 경우 true
//        System.out.println("[" + emailStr + "] : " + match.find());

    }
}

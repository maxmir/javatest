package RegEx;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegExpTest {
    public static void main(String[] args) {
        Pattern p = Pattern.compile("\\*\\t*");
        Matcher m = p.matcher("bat2\t");
        System.out.println(m.matches());
    }
}

/** 
 * @author jhkim
 * @date [2017. 8. 11.] 오후 4:25:32
 * RandomValue.java
 * 
 * modify
 * @date [2017. 10. 12.] 오후 4:46:42
 */

package Calcurate;

import java.util.Random;

public class RandomValue {
    public static void main(String[] args) {
        for (int i = 0; i < 10; i++) {
//            double dr = Math.random();
//            System.out.println("============================");
//            System.out.println(dr);
//            System.out.println(dr * 51);
//            System.out.println(dr * 51 + 50);
//            System.out.println((int) (dr * 51) + 50);  // (0.0 이상 ~ 1.0 미만 double) * (최대값 - 최소값 + 1) + 최소값
//            System.out.println("============================\n\n");
            
            
//            int maxVal = 100;
//            int minVal = (int) (Double.parseDouble("0.5") * 100);
//            // minVal과 maxVal사이의 임의값 검출 - ex) minCq가 '0.5'일때 CQ 50% ~ 100% 사이의 임의값 구함
//            int curCqPercent = (int) (Math.random() * (maxVal - minVal + 1)) + minVal;
//            System.out.println("curCqPercent = " + curCqPercent);
//            System.out.println(" /100 = " + (double) curCqPercent / 100);
            
//            System.out.println((int) (Math.random()*100000));
            Random r = new Random();
            System.out.println(r.nextInt(5));
        }
    }
}

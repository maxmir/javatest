package dart;

import java.util.ArrayList;
import java.util.Scanner;

/**
  * @FileName ArrangeCalc_01.java
  * @Project JavaTest
  * @since 2020. 3. 9. 오전 11:11:53
  * @author H1910047
  * @Description
  *     === 다트 마무리 점수 계산 ===
  *     1. goalScore에서 계산할 점수 입력받음
  *     2. targetList 생성하여 계산할 각 점수 목록 생성
  *         - singleTargetList
  *         - doubleTargetList
  *         - tripleTargetList
  *     3. calculationDarts ArrayList<Integer>에 계산할 점수들을 하나씩 넣으면서 더해보고 goalScore와 같은 점수가 나오는지 검사
  *     4. 만약 같은 점수가 나온다면 arrangeList ArrayList<ArrayList<Integer>> 에 추가해나감
  *     5. 모두 완료되면 출력 
  */                                                                          
public class ArrangeCalc_01 {
    public static void main(String[] args) {
        ArrangeCalc_01 arrangeCalc_01 = new ArrangeCalc_01();
        arrangeCalc_01.run();
    }

    /**
     * @author H1910047
     * @since 2020. 3. 9. 오후 1:39:24
     * @Description 
     */
    private void run() {
        /* 1. goalScore에서 계산할 점수 입력받음 */
        int goalScore = getGoalScore();
        
        /* 2. targetList 생성하여 계산할 각 점수 목록 생성 */
        ArrayList<ArrayList<String>> eachTargetList = getEachTargetList();
        
        /* 어레인지 목록 가져오기 */
        ArrayList<ArrayList<String>> arrangeList = getArrangeList(goalScore, eachTargetList);
    }

    /**
     * @author H1910047
     * @since 2020. 3. 10. 오후 3:15:14
     * @Description 
     *  2. targetList 생성하여 계산할 각 점수 목록 생성
     *   - singleTargetList
     *   - doubleTargetList
     *   - tripleTargetList
     * @return
     */
    private ArrayList<ArrayList<String>> getEachTargetList() {
        ArrayList<ArrayList<String>> eachTargetList = new ArrayList<>();
        ArrayList<String> singleTargetList = singleTargetList();
        ArrayList<String> doubleTargetList = doubleTargetList();
        ArrayList<String> tripleTargetList = tripleTargetList();
        eachTargetList.add(singleTargetList);
        eachTargetList.add(doubleTargetList);
        eachTargetList.add(tripleTargetList);
        return eachTargetList;
    }

    /**
     * @author H1910047
     * @since 2020. 3. 10. 오후 2:39:30
     * @Description 
     *      3. 어레인지 목록 가져오기
     *      - calculationDarts ArrayList<Integer>에 계산할 점수들을 하나씩 넣으면서 더해보고 goalScore와 같은 점수가 나오는지 검사
     *      - 만약 같은 점수가 나온다면 arrangeList ArrayList<ArrayList<Integer>> 에 추가해나감
     * @param goalScore 
     * @param eachTargetList 
     * @return
     */
    private ArrayList<ArrayList<String>> getArrangeList(int goalScore, ArrayList<ArrayList<String>> eachTargetList) {
        ArrayList<String> calculationDarts = new ArrayList<>();
        ArrayList<ArrayList<String>> arrangeListOf1Dart = new ArrayList<>();
        ArrayList<ArrayList<String>> arrangeListOf2Dart = new ArrayList<>();
        ArrayList<ArrayList<String>> arrangeListOf3Dart = new ArrayList<>();
        ArrayList<ArrayList<String>> arrangeList = new ArrayList<>();
        
        for (ArrayList<String> firstTargetList : eachTargetList) {
            for (String firstTarget : firstTargetList) { /* 첫번째 다트 점수 */
                int firstTargetScore = getTargetScore(firstTarget);
                if (firstTargetScore > goalScore) break;
                if (goalScore == firstTargetScore) {
                    calculationDarts.add(firstTarget);
                    arrangeListOf1Dart.add(calculationDarts);
                    calculationDarts = new ArrayList<>();
                    break;
                }
                else {
                    for (ArrayList<String> secoundTargetList : eachTargetList) {
                        for (String secoundTarget : secoundTargetList) { /* 두번째 다트 점수 */
                            int secoundTargetScore = getTargetScore(secoundTarget);
                            if ( (firstTargetScore + secoundTargetScore) > goalScore ) break;
                            if (goalScore == (firstTargetScore + secoundTargetScore)) {
                                calculationDarts.add(firstTarget);
                                calculationDarts.add(secoundTarget);
                                arrangeListOf2Dart.add(calculationDarts);
                                calculationDarts = new ArrayList<>();
                                break;
                            }
                            else {
                                for (ArrayList<String> thirdTargetList : eachTargetList) {
                                    for (String thirdTarget : thirdTargetList) { /* 세번째 다트 점수 */
                                        int thirdTargetScore = getTargetScore(thirdTarget);
                                        if ( (firstTargetScore + secoundTargetScore + thirdTargetScore) > goalScore) break;
                                        if (goalScore == (firstTargetScore + secoundTargetScore + thirdTargetScore)) {
                                            calculationDarts.add(firstTarget);
                                            calculationDarts.add(secoundTarget);
                                            calculationDarts.add(thirdTarget);
                                            arrangeListOf3Dart.add(calculationDarts);
                                            calculationDarts = new ArrayList<>();
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        System.out.println(arrangeListOf1Dart);
        System.out.println(arrangeListOf2Dart);
        System.out.println(arrangeListOf3Dart);
        arrangeList.addAll(arrangeListOf1Dart);
        arrangeList.addAll(arrangeListOf2Dart);
        arrangeList.addAll(arrangeListOf3Dart);
        return arrangeList;
    }

    /**
     * @author H1910047
     * @since 2020. 3. 10. 오후 3:51:31
     * @Description 문자열로 된 다트 점수를 숫자로 계산하여 변환
     * @param firstTarget
     * @return
     */
    private int getTargetScore(String targetScoreStr) {
        int targetScore = 0;
        if (targetScoreStr.contains("D") == false && targetScoreStr.contains("T") == false) {  // 싱글 점수이면 바로 숫자로 변환
            targetScore = Integer.parseInt(targetScoreStr);
        }
        else if (targetScoreStr.contains("D")) {  // 더블 점수이면 2배 계산하여 변환
            targetScore = Integer.parseInt(targetScoreStr.replace("D", "")) * 2;
        }
        else if (targetScoreStr.contains("T")) {  // 트리플 점수이면 3배 계산하여 변환
            targetScore = Integer.parseInt(targetScoreStr.replace("T", "")) * 3;
        }
        return targetScore;
    }

    /**
     * @author H1910047
     * @since 2020. 3. 9. 오후 1:44:04
     * @Description singleTargetList 생성하여 계산할 각 점수 목록 생성
     * @return
     */
    private ArrayList<String> singleTargetList() {
        ArrayList<String> singleTargetList = new ArrayList<>();
        for (int i = 0; i < 20; i++) {
            singleTargetList.add( (i + 1) + "" );
        }
        return singleTargetList;
    }
    
    /**
     * @author H1910047
     * @since 2020. 3. 9. 오후 1:44:06
     * @Description doubleTargetList 생성하여 계산할 각 점수 목록 생성
     * @return
     */
    private ArrayList<String> doubleTargetList() {
        ArrayList<String> doubleTargetList = new ArrayList<>();
        for (int i = 0; i < 20; i++) {
            doubleTargetList.add( (i + 1) + "D" );
        }
        return doubleTargetList;
    }

    /**
     * @author H1910047
     * @since 2020. 3. 9. 오후 1:44:08
     * @Description tripleTargetList 생성하여 계산할 각 점수 목록 생성
     * @return
     */
    private ArrayList<String> tripleTargetList() {
        ArrayList<String> tripleTargetList = new ArrayList<>();
        for (int i = 0; i < 20; i++) {
            tripleTargetList.add( (i + 1) + "T" );
        }
        return tripleTargetList;
    }

    /**
     * @author H1910047
     * @since 2020. 3. 9. 오후 1:39:42
     * @Description 1. goalScore에서 계산할 점수 입력받음
     * @return
     */
    private int getGoalScore() {
        int goalScore = 0;
        Scanner scanner = new Scanner(System.in);
        System.out.println("목표점수를 입력하세요.");
        goalScore = scanner.nextInt();
        System.out.printf("입력한 목표점수 : '%d'\n", goalScore);
        scanner.close();
        return goalScore;
    }
    
}

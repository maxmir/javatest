package dart;

import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;

/**
  * @FileName ArrangeCalc_05.java
  * @Project JavaTest
  * @since 2020. 3. 15. 오후 12:58:42
  * @author Kim Jong-heon
  * @Description
  *     === 다트 마무리 점수 계산 ===
  *     1. goalScore에서 계산할 점수 입력받음
  *     2. 마무리 모드 선택 입력 받음 - 'M': 마스터, 'D': 더블, 'T': 트리플, 'N 또는 비입력': 일반(모두허용)
  *     3. targetList 생성하여 계산할 각 점수 목록 생성
  *         - singleTargetList
  *         - doubleTargetList
  *         - tripleTargetList
  *     4. 어레인지 목록 가져오기
  *         - calculationDarts ArrayList<Integer>에 계산할 점수들을 하나씩 넣으면서 더해보고 goalScore와 같은 점수가 나오는지 검사
  *         - 만약 같은 점수가 나온다면 arrangeList ArrayList<ArrayList<Integer>> 에 추가해나감
  *
  * @modify
  * Kim Jong-heon(2020. 3. 13. 오후 2:47:57)
  * @Description 입력값 받는 방식 변경 (Scanner -> JOptionPane)
  */                                                                          
public class ArrangeCalc_06 extends JFrame {
    private static final long serialVersionUID = -2830673760484668207L;

    public static void main(String[] args) {
        ArrangeCalc_06 arrangeCalc_06 = new ArrangeCalc_06();
        arrangeCalc_06.run();
    }
    
//    private class ArrangeCalc_06 () {
//        
//    }

    /**
     * @author Kim Jong-heon
     * @since 2020. 3. 9. 오후 1:39:24
     * @Description 
     */
    private void run() {
//        /* 1. goalScore에서 계산할 점수 입력받음 */
//        int goalScore = getGoalScore();
//        
//        /* 2. 마무리 모드 선택 입력 받음 */
//        String finishMode = getFinisniMode();
        
        /* 원하는 마무리 다트 갯수 입력 받음 */
//        getFinishDartNum(goalScore, finishMode);
        
//        /* 3. targetList 생성하여 계산할 각 점수 목록 생성 */
//        ArrayList<ArrayList<String>> eachTargetList = getEachTargetList();
//        
//        /* 4. 어레인지 목록 가져오기 */
//        getArrangeList(goalScore, eachTargetList, finishMode);
    }

    /**
      * @FileName ArrangeCalc_04.java
      * @Project JavaTest
      * @since 2020. 3. 15. 오후 12:51:19
      * @author H1910047
      * @Description 원하는 마무리 다트 갯수 선택 체크박스 이너클래스
      */                                                                          
    private class FinishNumCheck extends JFrame {
        private static final long serialVersionUID = -7576299155981355611L;
        private JPanel contentPane;

        FinishNumCheck(int goalScore, String finishMode) {
            setTitle("마무리 다트 갯수 선택");
            setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            
            contentPane = new JPanel();
            contentPane.setBorder(new TitledBorder(null, "마무리 다트 갯수", TitledBorder.LEADING, TitledBorder.TOP, null, null));
            setContentPane(contentPane);
            contentPane.setLayout(null);
            
            Container c = getContentPane();
            c.setLayout(new FlowLayout());
                    
            JCheckBox oneFinish     = new JCheckBox("1");
            JCheckBox twoFinish     = new JCheckBox("2");
            JCheckBox threeFinish   = new JCheckBox("3");
            JCheckBox allFinish     = new JCheckBox("모두");
            JButton confirm = new JButton("확인");
            
            c.add(oneFinish);
            c.add(twoFinish);
            c.add(threeFinish);
            c.add(allFinish);
            c.add(confirm);
            
            confirm.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    boolean isOneChecked   = oneFinish.isSelected();
                    boolean isTwoChecked   = twoFinish.isSelected();
                    boolean isThreeChecked = threeFinish.isSelected();
                    
                  /* 3. targetList 생성하여 계산할 각 점수 목록 생성 */
                  ArrayList<ArrayList<String>> eachTargetList = getEachTargetList();
                  
                  /* 4. 어레인지 목록 가져오기 */
                  HashMap<String, ArrayList<ArrayList<String>>> arrangeListMap = getArrangeList(goalScore, eachTargetList, finishMode, isOneChecked, isTwoChecked, isThreeChecked);
                  
                  printArrangeList(arrangeListMap);
                  
                  dispose();
                }
            });
            
            setSize(250, 160);
            setVisible(true);
        }
    }
    
    
    /**
     * @author H1910047
     * @since 2020. 3. 15. 오후 12:00:23
     * @Description 원하는 마무리 다트 갯수 입력 받음
     * @param finishMode 
     * @param goalScore 
     * @return
     */
    private void getFinishDartNum(int goalScore, String finishMode) {
        new FinishNumCheck(goalScore, finishMode);
    }

    /**
     * @author Kim Jong-heon
     * @since 2020. 3. 13. 오후 1:56:54
     * @Description 2. 마무리 모드 선택
     * @return
     */
    private String getFinisniMode() {
        String[] finishModeButtons = {"마스터", "더블", "트리플", "모두허용"};
        int num = JOptionPane.showOptionDialog(null, "마무리 모드를 선택해주세요.(마스터, 더블, 트리플, 모두허용)", 
                "마무리 모드", JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE, null, finishModeButtons, "마스터");
        System.out.printf("선택한 마무리 모드는 '%s'입니다\n\n" , finishModeButtons[num]);
        
        switch (num) {
        case 1:
            return "M";  // 마스터
        case 2:
            return "D";  // 더블
        case 3:
            return "T";  // 트리플
        default:
            return "";   // 일반(모두허용)
        }
    }

    /**
     * @author Kim Jong-heon
     * @since 2020. 3. 10. 오후 5:56:24
     * @Description 2. 마무리 모드 선택
     * @return
     * 
     * @modify
     * H1910047(time)
     * @Description 
     */
    @SuppressWarnings({ "unused", "resource" })
    private String getFinisniModeByScanner() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("마무리 모드를 선택해주세요.('M': 마스터, 'D': 더블, 'T': 트리플, 'N 또는 비입력': 일반(모두허용))\n");
        String finishMode = scanner.nextLine();
        System.out.printf("선택한 마무리 모드는 '%s'입니다.\n", finishMode);
        return finishMode;
    }

    /**
     * @author Kim Jong-heon
     * @since 2020. 3. 10. 오후 3:15:14
     * @Description 
     *  3. targetList 생성하여 계산할 각 점수 목록 생성
     *   - singleTargetList
     *   - doubleTargetList
     *   - tripleTargetList
     * @return
     */
    private ArrayList<ArrayList<String>> getEachTargetList() {
        ArrayList<ArrayList<String>> eachTargetList = new ArrayList<>();
        ArrayList<String> singleTargetList = singleTargetList();
        ArrayList<String> doubleTargetList = doubleTargetList();
        ArrayList<String> tripleTargetList = tripleTargetList();
        eachTargetList.add(singleTargetList);
        eachTargetList.add(doubleTargetList);
        eachTargetList.add(tripleTargetList);
        return eachTargetList;
    }

    /**
     * @author Kim Jong-heon
     * @since 2020. 3. 10. 오후 2:39:30
     * @Description 
     *      4. 어레인지 목록 가져오기
     *      - calculationDarts ArrayList<Integer>에 계산할 점수들을 하나씩 넣으면서 더해보고 goalScore와 같은 점수가 나오는지 검사
     *      - 만약 같은 점수가 나온다면 arrangeList ArrayList<ArrayList<Integer>> 에 추가해나감
     * @param goalScore 
     * @param eachTargetList 
     * @param finishMode 
     * @param isOneChecked
     * @param isTwoChecked
     * @param isThreeChecked
     * @return
     */
    private HashMap<String, ArrayList<ArrayList<String>>> getArrangeList(int goalScore, ArrayList<ArrayList<String>> eachTargetList, String finishMode, boolean isOneChecked, boolean isTwoChecked, boolean isThreeChecked) {
        ArrayList<String> calculationDarts = new ArrayList<>();
        ArrayList<ArrayList<String>> arrangeListOf1Dart = new ArrayList<>();
        ArrayList<ArrayList<String>> arrangeListOf2Dart = new ArrayList<>();
        ArrayList<ArrayList<String>> arrangeListOf3Dart = new ArrayList<>();
//        ArrayList<ArrayList<String>> arrangeList = new ArrayList<>();
        HashMap<String, ArrayList<ArrayList<String>>> arrangeListMap = new HashMap<>();
        
        for (ArrayList<String> firstTargetList : eachTargetList) {
            for (String firstTarget : firstTargetList) {  // 첫번째 다트 점수
                int firstTargetScore = getTargetScore(firstTarget);
                if (firstTargetScore > goalScore) break;
                if (goalScore == firstTargetScore && isOneChecked) {  // 목표점수와 같고 마무리 갯수가 1개라면
                    if (isAllowed(firstTarget, finishMode) == false) continue;  // 모드에서 허용하는 타겟이 아니면 추가하지 않음
                    calculationDarts.add(firstTarget);
                    arrangeListOf1Dart.add(calculationDarts);
                    calculationDarts = new ArrayList<>();
                    break;
                }
                else {
                    for (ArrayList<String> secoundTargetList : eachTargetList) {
                        for (String secoundTarget : secoundTargetList) {  // 두번째 다트 점수
                            int secoundTargetScore = getTargetScore(secoundTarget);
                            if ( (firstTargetScore + secoundTargetScore) > goalScore ) break;
                            if (goalScore == (firstTargetScore + secoundTargetScore) && isTwoChecked) {  // 목표점수와 같고 마무리 갯수가 2개라면
                                if (isAllowed(secoundTarget, finishMode) == false) continue;  // 모드에서 허용하는 타겟이 아니면 추가하지 않음
                                calculationDarts.add(firstTarget);
                                calculationDarts.add(secoundTarget);
                                arrangeListOf2Dart.add(calculationDarts);
                                calculationDarts = new ArrayList<>();
                                break;
                            }
                            else {
                                for (ArrayList<String> thirdTargetList : eachTargetList) {
                                    for (String thirdTarget : thirdTargetList) { // 세번째 다트 점수
                                        int thirdTargetScore = getTargetScore(thirdTarget);
                                        if ( (firstTargetScore + secoundTargetScore + thirdTargetScore) > goalScore) break;
                                        if (goalScore == (firstTargetScore + secoundTargetScore + thirdTargetScore) && isThreeChecked) {  // 목표점수와 같고 마무리 갯수가 3개라면
                                            if (isAllowed(thirdTarget, finishMode) == false) continue;  // 모드에서 허용하는 타겟이 아니면 추가하지 않음
                                            calculationDarts.add(firstTarget);
                                            calculationDarts.add(secoundTarget);
                                            calculationDarts.add(thirdTarget);
                                            arrangeListOf3Dart.add(calculationDarts);
                                            calculationDarts = new ArrayList<>();
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
//        printArrangeList(arrangeListOf1Dart, arrangeListOf2Dart, arrangeListOf3Dart);
        
        arrangeListMap.put("arrangeListOf1Dart", arrangeListOf1Dart);
        arrangeListMap.put("arrangeListOf2Dart", arrangeListOf2Dart);
        arrangeListMap.put("arrangeListOf3Dart", arrangeListOf3Dart);
        
        
//        arrangeList.addAll(arrangeListOf1Dart);
//        arrangeList.addAll(arrangeListOf2Dart);
//        arrangeList.addAll(arrangeListOf3Dart);
        return arrangeListMap;
    }

    
    /**
     * @author H1910047
     * @since 2020. 3. 24. 오후 6:14:59
     * @Description 마무리 목록 출력 Override
     * @param arrangeListMap
     */
    private void printArrangeList(HashMap<String, ArrayList<ArrayList<String>>> arrangeListMap) {
        ArrayList<ArrayList<String>> arrangeListOf1Dart = arrangeListMap.get("arrangeListOf1Dart");
        ArrayList<ArrayList<String>> arrangeListOf2Dart = arrangeListMap.get("arrangeListOf2Dart");
        ArrayList<ArrayList<String>> arrangeListOf3Dart = arrangeListMap.get("arrangeListOf3Dart");
        printArrangeList(arrangeListOf1Dart, arrangeListOf2Dart, arrangeListOf3Dart);
    }
    
    /**
     * @author Kim Jong-heon
     * @since 2020. 3. 12. 오후 9:57:15
     * @Description 마무리 목록 출력
     * @param arrangeListOf1Dart
     * @param arrangeListOf2Dart
     * @param arrangeListOf3Dart
     */
    private void printArrangeList(ArrayList<ArrayList<String>> arrangeListOf1Dart, ArrayList<ArrayList<String>> arrangeListOf2Dart, ArrayList<ArrayList<String>> arrangeListOf3Dart) {
        int numberOfOneLine = 9;
        
        System.out.println("1발 마무리 목록");
        System.out.println("1발 마무리 갯수 : " + arrangeListOf1Dart.size());
        for (int i = 0; i < arrangeListOf1Dart.size(); i++) {
            System.out.print(setDigitNum(i, arrangeListOf1Dart.size()) + ": " + arrangeListOf1Dart.get(i) + "\t");
            if ( (i + 1) % numberOfOneLine == 0 ) {
                System.out.println();
            }
        }
        System.out.println("\n");
        
        System.out.println("2발 마무리 목록");
        System.out.println("2발 마무리 갯수 : " + arrangeListOf2Dart.size());
        for (int i = 0; i < arrangeListOf2Dart.size(); i++) {
            System.out.print(setDigitNum(i + 1, arrangeListOf2Dart.size()) + ": " + arrangeListOf2Dart.get(i) + "\t");
            if ( (i + 1) % numberOfOneLine == 0 ) {
                System.out.println();
            }
        }
        System.out.println("\n");
        
        System.out.println("3발 마무리 목록");
        System.out.println("3발 마무리 갯수 : " + arrangeListOf3Dart.size());
        for (int i = 0; i < arrangeListOf3Dart.size(); i++) {
            System.out.print(setDigitNum(i + 1, arrangeListOf3Dart.size()) + ": " + arrangeListOf3Dart.get(i) + "\t");
            if ( (i + 1) % numberOfOneLine == 0 ) {
                System.out.println();
            }
        }
    }

    /**
     * @author H1910047
     * @since 2020. 3. 13. 오후 12:31:53
     * @Description 자리수 맞추기
     * @param orderNum
     * @param size
     * @return
     */
    private String setDigitNum(int orderNum, int size) {
        int digitNum = String.valueOf(size).length();
        int orderNumDigit = String.valueOf(orderNum).length();
        
        String fillZero = "";
        if (orderNumDigit < digitNum) {
            for (int j = 0; j < digitNum - orderNumDigit; j++) {
                fillZero += "0";
            }
        }
        return fillZero + orderNum;
    }

    /**
     * @author Kim Jong-heon
     * @since 2020. 3. 12. 오전 10:43:46
     * @Description 선택 마무리 모드에 따른 목록 포함여부 결과 반환 (true - 포함허용, false - 포함비허용)  
     * @param target
     * @param finishMode
     * @return
     */
    private boolean isAllowed(String target, String finishMode) {
        if (finishMode.equalsIgnoreCase("") || finishMode.equalsIgnoreCase("N")) {  // 일반 마무리는 모두 허용
            return true;
        }
        else if (finishMode.equalsIgnoreCase("M")) {  // 마스터(더블, 트리플) 마무리일때는 더블, 트리플만 허용
            if (target.contains("D") || target.contains("T")) return true;
        }
        else if (finishMode.equalsIgnoreCase("D")) {  // 더블 마무리일때는 더블만 허용
            if (target.contains("D")) return true;
        }
        else if (finishMode.equalsIgnoreCase("T")) {  // 트리플 마무리일때는 트리플만 허용
            if (target.contains("T")) return true;
        }
        return false;
    }

    /**
     * @author Kim Jong-heon
     * @since 2020. 3. 10. 오후 3:51:31
     * @Description 문자열로 된 다트 점수를 숫자로 계산하여 변환
     * @param firstTarget
     * @return
     */
    private int getTargetScore(String targetScoreStr) {
        int targetScore = 0;
        if (targetScoreStr.contains("D") == false && targetScoreStr.contains("T") == false) {  // 싱글 점수이면 바로 숫자로 변환
            targetScore = Integer.parseInt(targetScoreStr);
        }
        else if (targetScoreStr.contains("D")) {  // 더블 점수이면 2배 계산하여 변환
            targetScore = Integer.parseInt(targetScoreStr.replace("D", "")) * 2;
        }
        else if (targetScoreStr.contains("T")) {  // 트리플 점수이면 3배 계산하여 변환
            targetScore = Integer.parseInt(targetScoreStr.replace("T", "")) * 3;
        }
        return targetScore;
    }

    /**
     * @author Kim Jong-heon
     * @since 2020. 3. 9. 오후 1:44:04
     * @Description singleTargetList 생성하여 계산할 각 점수 목록 생성
     * @return
     */
    private ArrayList<String> singleTargetList() {
        ArrayList<String> singleTargetList = new ArrayList<>();
        for (int i = 0; i < 20; i++) {
            singleTargetList.add( (i + 1) + "" );
        }
        return singleTargetList;
    }
    
    /**
     * @author Kim Jong-heon
     * @since 2020. 3. 9. 오후 1:44:06
     * @Description doubleTargetList 생성하여 계산할 각 점수 목록 생성
     * @return
     */
    private ArrayList<String> doubleTargetList() {
        ArrayList<String> doubleTargetList = new ArrayList<>();
        for (int i = 0; i < 20; i++) {
            doubleTargetList.add( (i + 1) + "D" );
        }
        return doubleTargetList;
    }

    /**
     * @author Kim Jong-heon
     * @since 2020. 3. 9. 오후 1:44:08
     * @Description tripleTargetList 생성하여 계산할 각 점수 목록 생성
     * @return
     */
    private ArrayList<String> tripleTargetList() {
        ArrayList<String> tripleTargetList = new ArrayList<>();
        for (int i = 0; i < 20; i++) {
            tripleTargetList.add( (i + 1) + "T" );
        }
        return tripleTargetList;
    }

    /**
     * @author Kim Jong-heon
     * @since 2020. 3. 9. 오후 1:39:42
     * @Description 1. goalScore에서 계산할 점수 입력받음
     * @return
     * 
     * @modify
     * Kim Jong-heon(2020. 3. 13. 오후 1:50:58)
     * @Description 메서드 명 변경 (getGoalScore -> getGoalScoreByScanner)
     */
    @SuppressWarnings({ "unused", "resource" })
    private int getGoalScoreByScanner() {
        int goalScore = 0;
        Scanner scanner = new Scanner(System.in);
        System.out.println("목표점수를 입력하세요.");
        goalScore = scanner.nextInt();
        System.out.printf("입력한 목표점수 : '%d'\n", goalScore);
        return goalScore;
    }
    
    /**
     * @author Kim Jong-heon
     * @since 2020. 3. 13. 오후 1:51:49
     * @Description 1. goalScore에서 계산할 점수 입력받음
     * @return
     */
    private int getGoalScore() {
        String goalScore = JOptionPane.showInputDialog("목표점수를 입력하세요. (2 ~ 180 사이의 숫자를 입력하세요.");
        System.out.println("입력한 목표점수는 '" + goalScore + "' 입니다.");
        return Integer.parseInt(goalScore);
    }
    
}

package dart;

import java.util.ArrayList;
import java.util.Scanner;

/**
  * @FileName ArrangeCalc_02.java
  * @Project JavaTest
  * @since 2020. 3. 10. 오후 6:01:37
  * @author kjh
  * @Description
  *     === 다트 마무리 점수 계산 ===
  *     1. goalScore에서 계산할 점수 입력받음
  *     2. targetList 생성하여 계산할 각 점수 목록 생성
  *         - singleTargetList
  *         - doubleTargetList
  *         - tripleTargetList
  *     3. calculationDarts ArrayList<Integer>에 계산할 점수들을 하나씩 넣으면서 더해보고 goalScore와 같은 점수가 나오는지 검사
  *     4. 만약 같은 점수가 나온다면 arrangeList ArrayList<ArrayList<Integer>> 에 추가해나감
  *     5. 모두 완료되면 출력
  */
public class ArrangeCalc_02 {
    public static void main(String[] args) {
        ArrangeCalc_02 arrangeCalc_02 = new ArrangeCalc_02();
        arrangeCalc_02.run();
    }

    /**
     * @author kjh
     * @since 2020. 3. 9. 오후 1:39:24
     * @Description
     */
    private void run() {
        /* 1. goalScore에서 계산할 점수 입력받음 */
        int goalScore = getGoalScore();

        /* 마무리 선택 입력 받음 */
        String finishMode = getFinisniMode();

        /* 2. targetList 생성하여 계산할 각 점수 목록 생성 */
        ArrayList<ArrayList<String>> eachTargetList = getEachTargetList();

        /* 어레인지 목록 가져오기 */
        ArrayList<ArrayList<String>> arrangeList = getArrangeList(goalScore, eachTargetList, finishMode);
    }

    /**
     * @author kjh
     * @since 2020. 3. 10. 오후 5:56:24
     * @Description 마무리 모드 선택
     * @return
     */
    private String getFinisniMode() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("마무리 모드를 선택해주세요.('M': 마스터, 'D': 더블, 'T': 트리플, 'N 또는 비입력': 일반(모두허용))\n");
        String finishMode = scanner.nextLine();
        System.out.printf("선택한 마무리 모드는 '%s'입니다.\n", finishMode);
        scanner.close();
        return finishMode;
    }

    /**
     * @author kjh
     * @since 2020. 3. 10. 오후 3:15:14
     * @Description
     *  2. targetList 생성하여 계산할 각 점수 목록 생성
     *   - singleTargetList
     *   - doubleTargetList
     *   - tripleTargetList
     * @return
     */
    private ArrayList<ArrayList<String>> getEachTargetList() {
        ArrayList<ArrayList<String>> eachTargetList = new ArrayList<>();
        ArrayList<String> singleTargetList = singleTargetList();
        ArrayList<String> doubleTargetList = doubleTargetList();
        ArrayList<String> tripleTargetList = tripleTargetList();
        eachTargetList.add(singleTargetList);
        eachTargetList.add(doubleTargetList);
        eachTargetList.add(tripleTargetList);
        return eachTargetList;
    }

    /**
     * @author kjh
     * @since 2020. 3. 10. 오후 2:39:30
     * @Description
     *      3. 어레인지 목록 가져오기
     *      - calculationDarts ArrayList<Integer>에 계산할 점수들을 하나씩 넣으면서 더해보고 goalScore와 같은 점수가 나오는지 검사
     *      - 만약 같은 점수가 나온다면 arrangeList ArrayList<ArrayList<Integer>> 에 추가해나감
     * @param goalScore
     * @param eachTargetList
     * @param finishMode
     * @return
     */
    private ArrayList<ArrayList<String>> getArrangeList(int goalScore, ArrayList<ArrayList<String>> eachTargetList, String finishMode) {
        ArrayList<String> calculationDarts = new ArrayList<>();
        ArrayList<ArrayList<String>> arrangeListOf1Dart = new ArrayList<>();
        ArrayList<ArrayList<String>> arrangeListOf2Dart = new ArrayList<>();
        ArrayList<ArrayList<String>> arrangeListOf3Dart = new ArrayList<>();
        ArrayList<ArrayList<String>> arrangeList = new ArrayList<>();

        for (ArrayList<String> firstTargetList : eachTargetList) {
            for (String firstTarget : firstTargetList) {  // 첫번째 다트 점수
                int firstTargetScore = getTargetScore(firstTarget);
                if (firstTargetScore > goalScore) break;
                if (goalScore == firstTargetScore) {  // 목표점수와 같다면
                    if (isAllowed(firstTarget, finishMode) == false) continue;  // 모드에서 허용하는 타겟이 아니면 추가하지 않음
                    calculationDarts.add(firstTarget);
                    arrangeListOf1Dart.add(calculationDarts);
                    calculationDarts = new ArrayList<>();
                    break;
                }
                else {
                    for (ArrayList<String> secoundTargetList : eachTargetList) {
                        for (String secoundTarget : secoundTargetList) {  // 두번째 다트 점수
                            int secoundTargetScore = getTargetScore(secoundTarget);
                            if ( (firstTargetScore + secoundTargetScore) > goalScore ) break;
                            if (goalScore == (firstTargetScore + secoundTargetScore)) {  // 목표점수와 같다면
                                if (isAllowed(secoundTarget, finishMode) == false) continue;  // 모드에서 허용하는 타겟이 아니면 추가하지 않음
                                calculationDarts.add(firstTarget);
                                calculationDarts.add(secoundTarget);
                                arrangeListOf2Dart.add(calculationDarts);
                                calculationDarts = new ArrayList<>();
                                break;
                            }
                            else {
                                for (ArrayList<String> thirdTargetList : eachTargetList) {
                                    for (String thirdTarget : thirdTargetList) { // 세번째 다트 점수
                                        int thirdTargetScore = getTargetScore(thirdTarget);
                                        if ( (firstTargetScore + secoundTargetScore + thirdTargetScore) > goalScore) break;
                                        if (goalScore == (firstTargetScore + secoundTargetScore + thirdTargetScore)) {  // 목표점수와 같다면
                                            if (isAllowed(thirdTarget, finishMode) == false) continue;  // 모드에서 허용하는 타겟이 아니면 추가하지 않음
                                            calculationDarts.add(firstTarget);
                                            calculationDarts.add(secoundTarget);
                                            calculationDarts.add(thirdTarget);
                                            arrangeListOf3Dart.add(calculationDarts);
                                            calculationDarts = new ArrayList<>();
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        printArrangeList(arrangeListOf1Dart, arrangeListOf2Dart, arrangeListOf3Dart);

        arrangeList.addAll(arrangeListOf1Dart);
        arrangeList.addAll(arrangeListOf2Dart);
        arrangeList.addAll(arrangeListOf3Dart);
        return arrangeList;
    }

    /**
     * @author kjh
     * @since 2020. 3. 12. 오후 9:57:15
     * @Description 마무리 목록 출력
     * @param arrangeListOf1Dart
     * @param arrangeListOf2Dart
     * @param arrangeListOf3Dart
     */
    private void printArrangeList(ArrayList<ArrayList<String>> arrangeListOf1Dart, ArrayList<ArrayList<String>> arrangeListOf2Dart, ArrayList<ArrayList<String>> arrangeListOf3Dart) {

    	int numberByOneLine = 20;
    	System.out.println("1발 마무리 목록");
    	System.out.printf("1발 마무리 갯수: %d\n\n", arrangeListOf1Dart.size());
    	for (int i = 0; i < arrangeListOf1Dart.size(); i++) {
			if (i % numberByOneLine == 0)
				System.out.println();
			else
    			System.out.print(arrangeListOf1Dart.get(i));
		}
    	System.out.println("\n");

    	System.out.println("2발 마무리 목록");
    	System.out.printf("2발 마무리 갯수: %d\n\n", arrangeListOf2Dart.size());
    	for (int i = 0; i < arrangeListOf2Dart.size(); i++) {
    		if (i % numberByOneLine == 0 && i > 0)
    			System.out.println();
    		else
    			System.out.print(arrangeListOf2Dart.get(i));
    	}
    	System.out.println("\n");

    	System.out.println("3발 마무리 목록");
    	System.out.printf("3발 마무리 갯수: %d\n\n", arrangeListOf3Dart.size());
    	for (int i = 0; i < arrangeListOf3Dart.size(); i++) {
    		if (i % numberByOneLine == 0 && i > 0)
    			System.out.println();
    		else
    			System.out.print(arrangeListOf3Dart.get(i));
    	}
    }

    /**
     * @author kjh
     * @since 2020. 3. 12. 오전 10:43:46
     * @Description 선택 마무리 모드에 따른 목록 포함여부 결과 반환 (true - 포함허용, false - 포함비허용)
     * @param target
     * @param finishMode
     * @return
     */
    private boolean isAllowed(String target, String finishMode) {
        if (finishMode.equalsIgnoreCase("") || finishMode.equalsIgnoreCase("N")) {  // 일반 마무리는 모두 허용
            return true;
        }
        else if (finishMode.equalsIgnoreCase("M")) {  // 마스터(더블, 트리플) 마무리일때는 더블, 트리플만 허용
            if (target.contains("D") || target.contains("T")) return true;
        }
        else if (finishMode.equalsIgnoreCase("D")) {  // 더블 마무리일때는 더블만 허용
            if (target.contains("D")) return true;
        }
        else if (finishMode.equalsIgnoreCase("T")) {  // 트리플 마무리일때는 트리플만 허용
            if (target.contains("T")) return true;
        }
        return false;
    }

    /**
     * @author kjh
     * @since 2020. 3. 10. 오후 3:51:31
     * @Description 문자열로 된 다트 점수를 숫자로 계산하여 변환
     * @param firstTarget
     * @return
     */
    private int getTargetScore(String targetScoreStr) {
        int targetScore = 0;
        if (targetScoreStr.contains("D") == false && targetScoreStr.contains("T") == false) {  // 싱글 점수이면 바로 숫자로 변환
            targetScore = Integer.parseInt(targetScoreStr);
        }
        else if (targetScoreStr.contains("D")) {  // 더블 점수이면 2배 계산하여 변환
            targetScore = Integer.parseInt(targetScoreStr.replace("D", "")) * 2;
        }
        else if (targetScoreStr.contains("T")) {  // 트리플 점수이면 3배 계산하여 변환
            targetScore = Integer.parseInt(targetScoreStr.replace("T", "")) * 3;
        }
        return targetScore;
    }

    /**
     * @author kjh
     * @since 2020. 3. 9. 오후 1:44:04
     * @Description singleTargetList 생성하여 계산할 각 점수 목록 생성
     * @return
     */
    private ArrayList<String> singleTargetList() {
        ArrayList<String> singleTargetList = new ArrayList<>();
        for (int i = 0; i < 20; i++) {
            singleTargetList.add( (i + 1) + "" );
        }
        return singleTargetList;
    }

    /**
     * @author kjh
     * @since 2020. 3. 9. 오후 1:44:06
     * @Description doubleTargetList 생성하여 계산할 각 점수 목록 생성
     * @return
     */
    private ArrayList<String> doubleTargetList() {
        ArrayList<String> doubleTargetList = new ArrayList<>();
        for (int i = 0; i < 20; i++) {
            doubleTargetList.add( (i + 1) + "D" );
        }
        return doubleTargetList;
    }

    /**
     * @author kjh
     * @since 2020. 3. 9. 오후 1:44:08
     * @Description tripleTargetList 생성하여 계산할 각 점수 목록 생성
     * @return
     */
    private ArrayList<String> tripleTargetList() {
        ArrayList<String> tripleTargetList = new ArrayList<>();
        for (int i = 0; i < 20; i++) {
            tripleTargetList.add( (i + 1) + "T" );
        }
        return tripleTargetList;
    }

    /**
     * @author kjh
     * @since 2020. 3. 9. 오후 1:39:42
     * @Description 1. goalScore에서 계산할 점수 입력받음
     * @return
     */
    private int getGoalScore() {
        int goalScore = 0;
        Scanner scanner = new Scanner(System.in);
        System.out.println("목표점수를 입력하세요.");
        goalScore = scanner.nextInt();
        System.out.printf("입력한 목표점수 : '%d'\n", goalScore);
        return goalScore;
    }

}

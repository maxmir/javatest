package Collection;

import java.util.ArrayList;
import java.util.HashMap;


public class ListValueGetTest {
	public static void main(String[] args) {
		HashMap<String, String> map = new HashMap<>();
		map.put("001", "test1");
		map.put("003", "test2");
		map.put("004", "test3");
		
		ArrayList<String> al = new ArrayList<>();
		al.add("001:test1");
		al.add("002:test2");
		al.add("003:test3");
		
		System.out.println(al.toArray());
	}
}

package Collection;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class MapMerge {
    public static void main(String... args) {
        Map<String, Object> m1 = new HashMap<String, Object>();
        Map<String, Object> m2 = new HashMap<String, Object>();
        Map<String, Object> m3 = new HashMap<String, Object>();

        m1.put("1", "1aaa");
        m1.put("2", "1bbb");
        m1.put("4", "1eee");

        m2.put("1", "2ccc");
        m2.put("3", "2ddd");
        m2.put("4", "2fff");
        m2.put("5", null);

        m3.putAll(m1);
        m3.putAll(m2);
        System.out.println(m3);
    }
}

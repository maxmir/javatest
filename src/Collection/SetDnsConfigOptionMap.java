package Collection;

import java.util.HashMap;


public class SetDnsConfigOptionMap {
    private HashMap<String, HashMap<String, String>> setDnsConfigOptionMap(HashMap<String, String> nodeConfMap, HashMap<String, HashMap<String, Object>> idxManageMap) {
        String configRowIdx = "";
        String nodeId = "";
        String sceSeq = "";
        HashMap<String, HashMap<String, String>> configOptionRowMap = new HashMap<String, HashMap<String,String>>();
        HashMap<String, String> rowInfoMap = null;
        
        for (String key : nodeConfMap.keySet()) {
            if (key.equalsIgnoreCase("node_id")) {
//                nodeId = nodeConfMap.get(key);
            }
            else if (key.equalsIgnoreCase("sce_seq")) {
                sceSeq = nodeConfMap.get(key);
            }
            else if (key.contains("records_ip")) {
                configRowIdx = key.split("ip_")[1];
                rowInfoMap = configOptionRowMap.get(configRowIdx);
                
                if (rowInfoMap == null || rowInfoMap.size() <= 0) {
                    rowInfoMap = new HashMap<String, String>();
                    rowInfoMap.put("records_ip", nodeConfMap.get(key));
                    rowInfoMap.put("configRowIdx", configRowIdx);
                    configOptionRowMap.put(configRowIdx, rowInfoMap);
                }
                else {
                    rowInfoMap.put("records_ip", nodeConfMap.get(key));
                }
            }
            else if (key.contains("records_type")) {
                configRowIdx = key.split("type_")[1];
                rowInfoMap = configOptionRowMap.get(configRowIdx);
                
                if (rowInfoMap == null || rowInfoMap.size() <= 0) {
                    rowInfoMap = new HashMap<String, String>();
                    rowInfoMap.put("records_type", nodeConfMap.get(key));
                    rowInfoMap.put("configRowIdx", configRowIdx);
                    configOptionRowMap.put(configRowIdx, rowInfoMap);
                }
                else {
                    rowInfoMap.put("records_type", nodeConfMap.get(key));
                }
            }
            else if (key.contains("sub_domain_name")) {
                configRowIdx = key.split("name_")[1];
                rowInfoMap = configOptionRowMap.get(configRowIdx);
                
                if (rowInfoMap == null || rowInfoMap.size() <= 0) {
                    rowInfoMap = new HashMap<String, String>();
                    rowInfoMap.put("sub_domain_name", nodeConfMap.get(key));
                    rowInfoMap.put("configRowIdx", configRowIdx);
                    configOptionRowMap.put(configRowIdx, rowInfoMap);
                }
                else {
                    rowInfoMap.put("sub_domain_name", nodeConfMap.get(key));
                }
            }
        }
        
        setNodeId(configOptionRowMap, nodeId);
        setParentIdx(configOptionRowMap, idxManageMap);
        System.out.println(configOptionRowMap);
        return configOptionRowMap;
    }
    
    private void setNodeId(HashMap<String, HashMap<String, String>> configRowMap, String nodeId) {
        for (String key : configRowMap.keySet()) {
            HashMap<String, String> rowInfoMap = configRowMap.get(key);
            rowInfoMap.put("node_id", nodeId);
        }
    }
    
    private void setParentIdx(HashMap<String, HashMap<String, String>> configOptionRowMap, HashMap<String, HashMap<String, Object>> idxManageMap) {
        // TODO Auto-generated method stub
        
    }
    

    public static void main(String[] args) {
        HashMap<String, String> nodeConfMap = new HashMap<String, String>();
//        {records_ip_01=33, records_ip_02=66, sub_domain_name_01=11, records_type_01=22, sce_seq=1083, records_type_02=55, node_id=os-dns-0_config_1_addOption, sub_domain_name_02=44}
        nodeConfMap.put("node_id", "dns-0_config_1_addOption");
        nodeConfMap.put("sce_seq", "1083");
        nodeConfMap.put("records_ip_01", "records_ip_01");
        nodeConfMap.put("records_ip_02", "records_ip_02");
        nodeConfMap.put("records_type_01", "records_type_01");
        nodeConfMap.put("records_type_02", "records_type_02");
        nodeConfMap.put("sub_domain_name_01", "sub_domain_name_01");
        nodeConfMap.put("sub_domain_name_02", "sub_domain_name_02");
        
        HashMap<String, HashMap<String, String>> idxManageMap = new HashMap<String, HashMap<String, String>>();
//        HashMap<String, String> 
//        idxManageMap.put("os-dns-0_config", value)
        
//        SetDnsConfigOptionMap sdcm = new SetDnsConfigOptionMap();
//        HashMap<String, HashMap<String, String>> configManageMap2 = sdcm.setDnsConfigOptionMap(nodeConfMap, idxManageMap);
    }
}

/**
 * @author jhkim
 * LinkedMapTest.java
 * 2017. 7. 10.
 */
package Collection;

import java.util.LinkedHashMap;
import java.util.Map;

public class LinkedMapTest {
    public static void main(String[] args) {
//        Map<String, String> map = new HashMap<>();
        Map<String, String> map = new LinkedHashMap<>();
        map.put("key1", "1");
        map.put("key2", "2");
        map.put("key3", "3");
        map.put("key4", "4");
        map.put("key5", "5");
        map.put("key6", "6");
        map.put("key7", "7");
        map.put("key8", "8");
        map.put("key9", "9");
        map.put("key10", "10");
        map.put("key11", "11");
        map.put("key12", "12");
        
        System.out.println(map);
    }
}

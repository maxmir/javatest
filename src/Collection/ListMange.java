package Collection;

import java.util.ArrayList;
import java.util.List;


public class ListMange {
    void go() {
        List<String> list1 = new ArrayList<String>();
        List<String> list2 = new ArrayList<String>();
        List<String> mergeList = new ArrayList<String>();
        
        list1.add("1");
        list1.add("2");
        list1.add("3");
        list1.add("4");
        System.out.println(list1);
        
        list2.add("2-1");
        list2.add("2-2");
        list2.add("2-3");
        list2.add("2-4");
        list2.add("2-5");
        System.out.println(list2);
        
        mergeList.addAll(list1);
        System.out.println(mergeList);
        mergeList.addAll(list2);
        System.out.println(mergeList);
    }
    
    public static void main(String[] args) {
        ListMange lm = new ListMange();
        lm.go();
    }
}

package Collection;

import java.util.HashMap;


public class SetDnsConfigMap {
    private HashMap<String, HashMap<String, String>> setDnsConfigMap(HashMap<String, String> nodeConfMap) {
        String configRowIdx = "";
        String nodeId = "";
        String sceSeq = "";
        HashMap<String, HashMap<String, String>> configRowMap = new HashMap<String, HashMap<String,String>>();
        HashMap<String, String> rowInfoMap = null;
        
        for (String key : nodeConfMap.keySet()) {
            if (key.equalsIgnoreCase("node_id")) {
                nodeId = nodeConfMap.get(key);
            }
            else if (key.equalsIgnoreCase("sce_seq")) {
                sceSeq = nodeConfMap.get(key);
            }
            else if (key.contains("domain_name")) {
                configRowIdx = key.split("name_")[1];
                rowInfoMap = configRowMap.get(configRowIdx);
                
                if (rowInfoMap == null || rowInfoMap.size() <= 0) {
                    rowInfoMap = new HashMap<String, String>();
                    rowInfoMap.put("domain_name", nodeConfMap.get(key));
                    rowInfoMap.put("configRowIdx", configRowIdx);
                    configRowMap.put(configRowIdx, rowInfoMap);
                }
                else {
                    rowInfoMap.put("domain_name", nodeConfMap.get(key));
                }
            }
            else if (key.contains("domain_ip")) {
                configRowIdx = key.split("ip_")[1];
                rowInfoMap = configRowMap.get(configRowIdx);
                
                if (rowInfoMap == null || rowInfoMap.size() <= 0) {
                    rowInfoMap = new HashMap<String, String>();
                    rowInfoMap.put("domain_ip", nodeConfMap.get(key));
                    rowInfoMap.put("configRowIdx", configRowIdx);
                    configRowMap.put(configRowIdx, rowInfoMap);
                }
                else {
                    rowInfoMap.put("domain_ip", nodeConfMap.get(key));
                }
            }
        }
        
        setNodeId(configRowMap, nodeId);
        setSceSeq(configRowMap, sceSeq);
        System.out.println("configRowMap- " + configRowMap);
        return configRowMap;
    }
    
    private void setNodeId(HashMap<String, HashMap<String, String>> configRowMap, String nodeId) {
        for (String key : configRowMap.keySet()) {
            HashMap<String, String> rowInfoMap = configRowMap.get(key);
            rowInfoMap.put("node_id", nodeId);
        }
    }
    
    private void setSceSeq(HashMap<String, HashMap<String, String>> configRowMap, String sceSeq) {
        for (String key : configRowMap.keySet()) {
            HashMap<String, String> rowInfoMap = configRowMap.get(key);
            rowInfoMap.put("sce_seq", sceSeq);
        }
    }
    
    private void insertDnsInfoAll(HashMap<String, HashMap<String, String>> configManageMap2) {
        HashMap<String, HashMap<String, String>> idxManageMap = new HashMap<String, HashMap<String,String>>();
        HashMap<String, String> rowIdxMap = null;
        int cnt = 0;
        
        for (String key : configManageMap2.keySet()) {
            HashMap<String, String> rowInfoMap = configManageMap2.get(key);
//            scenarioManageService.insertDnsInfo(rowInfoMap);
            String nodeId = rowInfoMap.get("node_id");
            String idx = ++cnt + "";
            String configRowIdx = rowInfoMap.get("configRowIdx");
            
            rowIdxMap = idxManageMap.get(nodeId);
            if (rowIdxMap != null) {
                rowIdxMap = idxManageMap.get(nodeId);
                rowIdxMap.put(configRowIdx, idx);
            }
            else {
                
                rowIdxMap = new HashMap<String, String>();
                rowIdxMap.put(configRowIdx, idx);
                idxManageMap.put(nodeId, rowIdxMap);
            }
        }
        System.out.println("idxManageMap- "+idxManageMap);
    }

    public static void main(String[] args) {
        HashMap<String, String> nodeConfMap = new HashMap<String, String>();
        nodeConfMap.put("node_id", "dns-0_config");
        nodeConfMap.put("sce_seq", "1045");
        nodeConfMap.put("domain_ip_01", "domain_ip_01");
        nodeConfMap.put("domain_ip_02", "domain_ip_02");
        nodeConfMap.put("domain_name_01", "domain_name_01");
        nodeConfMap.put("domain_name_02", "domain_name_02");
        
        SetDnsConfigMap sdcm = new SetDnsConfigMap();
        HashMap<String, HashMap<String, String>> configManageMap2 = sdcm.setDnsConfigMap(nodeConfMap);
        sdcm.insertDnsInfoAll(configManageMap2);
    }
}

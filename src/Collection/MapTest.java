/** 
 * @author jhkim
 * @date [2017. 8. 11.] 오후 3:40:03
 * MapTest.java
 */
package Collection;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class MapTest {
    public static void main(String[] args) {
        Map<String, Set<String>> map = new HashMap<>();
//        Set<String> set = new HashSet<>();
//        set.add("test10");
//        map.put("10%", set);
        
        Set<String> s1 = map.get("10%");
        s1 = new HashSet<>();
        s1.add("test17");
        System.out.println(map);
    }
}

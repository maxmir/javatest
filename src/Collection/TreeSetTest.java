/**
 * @author jhkim
 * TreeSetTest.java
 * 2017. 6. 19.
 */
package Collection;

import java.util.TreeSet;

public class TreeSetTest {
    public static void main(String[] args) {
        TreeSet<Long> uptime = new TreeSet<>();
        uptime.add(111111L);
        uptime.add(222222L);
        uptime.add(333333L);
        uptime.add(444444L);
        System.out.println(uptime);
        
        System.out.println(uptime.first());
        uptime.pollFirst();
        System.out.println(uptime);
        System.out.println(uptime.first());
        uptime.pollFirst();
        System.out.println(uptime);
        System.out.println(uptime.first());
        uptime.pollFirst();
        System.out.println(uptime);
        System.out.println(uptime.first());
    }
}

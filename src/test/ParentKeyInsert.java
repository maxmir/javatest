package test;

import java.util.ArrayList;
import java.util.Arrays;


public class ParentKeyInsert {
    public void setParentTreeKeys() {
        
        String treeKeys = "030102,030101,04020201";
        String[] treeKeysArr = treeKeys.split(",");
        ArrayList<String> treeKeysList = new ArrayList<String>(Arrays.asList(treeKeysArr));
        System.out.println(treeKeysList);
        
        String[] setParentTreeKeysArr = insertParentKey(treeKeysList);
        for (String string : setParentTreeKeysArr) {
            System.out.print(string+", ");
        }
    }
    
    private String[] insertParentKey(ArrayList<String> treeKeysList) {
        /*
         * treeKeys 자리수 검사
         * 반복
         *  만약 2자리를 초과한다면
         *  끝2자리를 자른 앞부분을 삽입
         */
        for (int i = 0; i < treeKeysList.size(); i++) {
            String treeKey = treeKeysList.get(i);
            int digit = treeKey.length();
            if (digit > 2) {
                String parentKey = treeKey.substring(0, treeKey.length() - 2);
                boolean isKey = keyFind(parentKey, treeKeysList);
                if (isKey == false) {
                    treeKeysList.add(parentKey);
                    insertParentKey(treeKeysList);
                }
                System.out.println("parentKey-- "+parentKey);
            }
        }
        
//       for (String treeKey : treeKeysList) {
//           int digit = treeKey.length();
//           if (digit > 2) {
//               String parentKey = treeKey.substring(0, treeKey.length() - 2);
//               boolean isKey = keyFind(parentKey, treeKeysList);
//               if (isKey == false) {
//                   treeKeysList.add(parentKey);
//                   insertParentKey(treeKeysList);
//               }
//               System.out.println("parentKey-- "+parentKey);
//           }
//       }
        
        return (String[]) treeKeysList.toArray(new String[treeKeysList.size()]);
    }

    private boolean keyFind(String parentKey, ArrayList<String> treeKeysList) {
        for (String treeKey : treeKeysList) {
            if (parentKey.equalsIgnoreCase(treeKey)) return true;
        }
        return false;
    }

    public static void main(String[] args) {
        ParentKeyInsert pki = new ParentKeyInsert();
        pki.setParentTreeKeys();
    }
}

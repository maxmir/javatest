package test;

/**
 * @author jhkim
 * TryCatchTest.java
 * 2017. 2. 22.
 */

public class TryCatchTest {
    int testmethod() {
        int i = 0;
        try {
            i = 1/0;
        } catch (Exception e) {
            e.printStackTrace();
            i = -1;
        }
        if (i == 0) System.out.println("---------------------------------------------------------------------------------------------------");
        return i;
    }
    
    public static void main(String[] args) {
        System.out.println("try");
        TryCatchTest tt = new TryCatchTest();
        for (int i = 0; i < 100; i++) {
            int res = tt.testmethod();
            System.out.println("ret -> '" + res + "'");
            System.out.println("\n\n\n");
        }
        System.out.println("end");
    }
}

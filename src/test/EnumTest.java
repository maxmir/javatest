package test;

public class EnumTest {
    // OS(윈도우, 리눅스, 유닉스, 웹서버, 메일서버, DNS, DB서버, 망연동서버, IDS/IPS), 방화벽, 위협발생기, 행위발생기, 라우터, NAT, 가상스위치(L2스위치), 물리스위치(실장비 연동스위치), 물리서버
    enum VmType {os_window, os_linux, os_unix, os_webserver, os_mail, os_dns, os_link, os_idsips, waf, attacker, behavier, router, nat, l2switch, physics_switch, physics_server}
    
    private void enumTest(String curNode) {
        VmType node = VmType.valueOf(curNode);
        switch (node) {
        case os_window:
            System.out.println("windowsNode");
            break;

        case os_linux:
            System.out.println("linuxNode");
            break;
            
        case os_webserver:
            System.out.println("webserverNode");
            break;
            
        case os_mail:
            System.out.println("mailNode");
            break;
            
        default:
            break;
        }
    }
    
    public static void main(String[] args) {
        EnumTest et = new EnumTest();
        String curNode = "os_linux".replace("-", "_");
        et.enumTest(curNode);
    }
}

package test;


public class IpNum {
    public static void main(String[] args) {
        int ipNum  = 168427779;
        String ip = ipNumToip(ipNum);
        System.out.println(ip);
        
        ipNum = ipToipNum("10.10.1.0");
        System.out.println(ipNum);
    }

    private static int ipToipNum(String ip) {
        String[] ipArr = ip.split("\\.");
        return (Integer.valueOf(ipArr[0]) << 24) + (Integer.valueOf(ipArr[1]) << 16) + (Integer.valueOf(ipArr[2]) << 8) + Integer.valueOf(ipArr[3]);
    }

    private static String ipNumToip(int ipNum) {
        return ((ipNum >> 24) & 0xFF) + "." + ((ipNum >> 16) & 0xFF) + "." + ((ipNum >> 8) & 0xFF) + "." + (ipNum & 0xFF);
    }
}

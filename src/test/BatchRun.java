package test;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 * @author jhkim
 * BatchRun.java
 * 2017. 2. 1.
 */
public class BatchRun {

    public void execute(String batchFile){

        Process proc = null;

        try {

            String[] command = {"casperjs /Users/eypark/Documents/workspace/screenCaptureSendMail/script/woongjin21.js"};
            proc = Runtime.getRuntime().exec(command);

//          proc = Runtime.getRuntime().exec(batchFile);

            BufferedReader br = new BufferedReader(new InputStreamReader(proc.getInputStream()));
            String line = null;

            while((line = br.readLine()) != null){
                System.out.println(line);
            }

            proc.getErrorStream().close();
            proc.getInputStream().close();
            proc.getOutputStream().close();
            proc.waitFor();

            int exit = proc.exitValue();
            System.out.println("exitValue " + exit);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

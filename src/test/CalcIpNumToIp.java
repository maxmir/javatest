package test;

/**
 * @author jhkim
 * CalcIpNumToIp
 * 2017. 2. 13.
 */
public class CalcIpNumToIp {
    public static void main(String... args) {
        double ipNum = Double.valueOf(168427776);

        for (int i = 0; i < 65279; i++) {
            ipNum++;
            System.out.println( ( ((long)ipNum >> 24) & 0xFF ) + "." + ( ((long)ipNum >> 16) & 0xFF ) + "." + ( ((long)ipNum >> 8) & 0xFF ) + "." + ( (long)ipNum & 0xFF ) );
        }

//        System.out.println((byte)((ipNum >> 24) & 0xFF));
//        System.out.println((byte)((ipNum >> 16) & 0xFF));
//        System.out.println((byte)((ipNum >> 8) & 0xFF));
//        System.out.println((byte)(ipNum & 0xFF));
    }
}

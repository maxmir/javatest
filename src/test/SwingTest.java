package test;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;


public class SwingTest implements ActionListener {
    JFrame frame;
    JButton button;
    JLabel label;
    
    public static void main(String[] args) {
        SwingTest st = new SwingTest();
        st.run();
    }
    
    public void run() {
        frame = new JFrame();
        button = new JButton();
        button.setText("click me");
        button.addActionListener(this);
        label = new JLabel();
        label.setText("show me");
        
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().add(BorderLayout.SOUTH, button);
        frame.getContentPane().add(BorderLayout.CENTER, label);
        frame.setSize(300, 300);
        frame.setVisible(true);
    }

    public void actionPerformed(ActionEvent e) {
        label.setText("text");
    }
}

package test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class NSXTestWithPython {
    public static void main(String[] args) {
        String process = null;
        String folderName = "python_file";
        String pythonFileName = "vm.py";
        String xmlFileName = "vm.xml";
        String pyFilePath = folderName + "/" + pythonFileName;
        String xmlFilePath = folderName + "/" + xmlFileName;
        
        try {
            Process p = Runtime.getRuntime().exec("python " + pyFilePath + " " + xmlFilePath);
//            Process p = Runtime.getRuntime().exec("python " + pyFilePath);
            BufferedReader input = new BufferedReader(new InputStreamReader(p.getInputStream()));
            while ((process = input.readLine()) != null) {
                System.out.println(process);
            }
            input.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
